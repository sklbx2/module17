#include <iostream>
#include <cmath>

class Vector
{
    double m_x{ 0 }, m_y{ 0 }, m_z{ 0 };

public:
    Vector()
    {}

    Vector(double x, double y, double z) : m_x(x), m_y(y), m_z(z)
    {
    }

    double getLength()
    {
        return sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
    }

    void print()
    {
        std::cout << m_x << ' ' << m_y << ' ' << m_z;
    }
};

int main()
{
    Vector a(15, 7, 4);

    std::cout << "Vector a: ";
    a.print();
    std::cout << std::endl;

    std::cout << "a length: " << a.getLength() << std::endl;

    return 0;
}

